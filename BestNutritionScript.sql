drop database if exists BestNutrition;

create database BestNutrition

use BestNutrition
go

/*
	De las personas
*/

create table Persona (
	id int primary key,
	primerNombre varchar(30) not null,
	segundoNombre varchar(30),
	secondGivenName varchar(30) not null,
	apellidoMaterno varchar(30),
	apellidoPaterno varchar(30),
	fechaNacimiento date,
	cuentaBancaria varchar(16)
);

create table Alergia (
	id int primary key,
	nombre varchar(30)
);

create table Enfermedad (
	id int primary key,
	nombre varchar(30)
);

create table PlanNutricional (
	id int primary key,
	nombre varchar(30),
	fechaConsulta Datetime,
	colaciones varchar(30),
	libre varchar(30)
);

create table Suscripcion (
	id int primary key,
	nombre varchar(30),
	tipo varchar(10),
	periodo varchar(10)
);

create table Ingrediente (
	id int primary key,
	nombre varchar(30),
	cantidad int,
	calorias bigint
);

create table [Usuario] (
	id int primary key,
	correo varchar(30),
	pass varchar(50),
	fk_persona        int foreign key (fk_persona) references Persona(id)
);

create table Rol (
	id int primary key,
	descripcion varchar(30),
	fk_auth            int,
	fk_cliente         int,
	fk_administrador   int,
	fk_nutriologo      int
);
alter table Rol add constraint fk_cliente foreign key(fk_auth) references Usuario(id)
alter table Rol add constraint fk_administrador foreign key(fk_administrador) references Administrador(id)
alter table Rol add constraint fk_nutriologo foreign key (fk_nutriologo) references Nutriologo(id)


create table Nutriologo (
	id int primary key,
	cedula bigint,
	fk_rol             int foreign key(fk_rol) references Rol(id),
	fk_persona         int foreign key(fk_persona) references Persona(id)
);

create table Administrador (
	id int primary key,
	nombre varchar(30),
	write bit,
	passwordAdmin varchar(30),
	fk_rol             int foreign key (fk_rol) references Rol(id),
	fk_persona         int foreign key (fk_persona) references Persona(id)
);

create table Auth (
	id int primary key,
	token varchar(50),
	passwordAuth varchar(30),
	fk_usuario         int foreign key (fk_usuario) references Usuario(id)
)

create table Cliente (
	id int primary key,
	tipoCuenta varchar(30),
	peso int,
	altura int,
	pass varchar(30),
	fk_persona         int foreign key (fk_persona) references Persona(id),
	fk_rol             int foreign key (fk_rol) references Rol(id),
	fk_planNutricional int foreign key (fk_planNutricional) references PlanNutricional(id),
);

create table ClienteAlergia (
	id int primary key,
	fk_cliente         int foreign key(fk_cliente) references Cliente(id),
	fk_alergia         int foreign key (fk_alergia) references Alergia(id)
);

create table ClienteEnfermedad (
	id int primary key,
	fk_cliente         int foreign key(fk_cliente) references Cliente(id),
	fk_enfermedad      int foreign key (fk_enfermedad) references Enfermedad(id)
);

create table ClienteSuscripcion (
	id int primary key,
	fk_cliente         int foreign key(fk_cliente) references Cliente(id),
	fk_suscripcion     int foreign key (fk_suscripcion) references Suscripcion(id)
);

create table Receta (
	id int primary key,
	nombre varchar(30),
	calorias bigint,
);

create table PlanIngrediente (
	id int primary key,
	fk_planNutricional int foreign key(fk_planNutricional) references PlanNutricional(id),
	fk_ingrediente     int foreign key(fk_ingrediente) references Ingrediente(id)
);


create table RecetaIngrediente (
	id int primary key,
	fk_receta          int foreign key(fk_receta) references Receta(id),
	fk_ingrediente     int foreign key (fk_ingrediente) references Ingrediente(id)
);
